<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <bean:message key="label.bookCreateEdit.title" locale="display" />
    </title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <link rel="stylesheet" href="css/theme.css" type="text/css" />

</head>

<body id="public">

<script language="JavaScript" type="text/javascript">
<!--
    function actionValue(value) {
        document.bookForm.action.value = value;
    }
//-->
</script>

<%@ page import="com.javachap.domain.Genre" %>
<%@ page import="java.util.List" %>

<div id="container">

    <%@ include file="/header_inc.jsp" %>

    <html:form action="/book.do" method="post" styleClass="javachap topLabel" styleId="form3">

        <html:hidden property="action"  />
        <html:hidden property="bookId" />

        <div class="info">
            <logic:empty name="bookForm" property="bookId">
                <h2>
                    <bean:message key="label.bookCreate.heading" locale="display" />
                </h2>
            </logic:empty>
            <logic:notEmpty name="bookForm" property="bookId">
                <h2>
                    <bean:message key="label.bookEdit.heading" locale="display" />
                </h2>
            </logic:notEmpty>
            <p>
               <bean:message key="label.bookCreateEdit.headingCaption" locale="display" />
            </p>

        </div>

        <logic:messagesPresent>
            <li id="errorLi">
            <h3 id="errorMsgLbl">
                <bean:message key="error.label.mainName" locale="display"/>
            </h3>
            <p id="errorMsg">
                <bean:message key="error.label.subName1" locale="display"/>
                <strong> <bean:message key="error.label.subName2" locale="display"/> </strong>
                <bean:message key="error.label.subName3" locale="display"/>
            </p>
            </li>
        </logic:messagesPresent>

        <ul>

            <li class="<logic:messagesPresent property='genre'>error</logic:messagesPresent>" id="fo3li7" >
                <label class="desc" id="title7" for="Field7">
                <bean:message key="label.genre" locale="display"/> <span id="req_111" class="req">*</span>
                </label>
                <div>
                    <html:select styleClass="field select medium" styleId="Field7" property="genre" tabindex="1" >
                        <html:option value="Select any Genre">
                            <bean:message key="label.option.default" locale="display" />
                        </html:option>
                        <html:optionsCollection name="genreList" label="name" value="id" />
                    </html:select>
                </div>
                <logic:messagesPresent property="genre">
                    <p class="error"><html:errors property="genre" locale="display"/></p>
                </logic:messagesPresent>
            </li>

            <li class="<logic:messagesPresent property='title'>error</logic:messagesPresent>" id="fo3li0" >
                <label class="desc" id="title0" for="Field0">
                <bean:message key="label.title" locale="display"/> <span id="req_111" class="req">*</span>
                </label>
                <div>
                    <html:text styleId="Field0" styleClass="field text medium" property="title"
                                tabindex="1" maxlength="255"/>
                </div>
                <logic:messagesPresent property="title">
                    <p class="error"><html:errors property="title" locale="display"/></p>
                </logic:messagesPresent>
            </li>

            <li class="<logic:messagesPresent property='description'>error</logic:messagesPresent>" id="fo3li0" >
                <label id="title1" class="desc" for="Field1">
                <bean:message key="label.description" locale="display"/> <span id="req_111" class="req">*</span>
                </label>
                <div>
                    <html:textarea styleId="Field1" styleClass="field textarea medium" property="description"
                                    rows="10" cols="50" tabindex="2" ></html:textarea>
                </div>
                <logic:messagesPresent property="description">
                    <p class="error"><html:errors property="description" locale="display"/></p>
                </logic:messagesPresent>
            </li>

            <li class="<logic:messagesPresent property='author'>error</logic:messagesPresent>" id="fo3li0" >
                <label class="desc" id="title4" for="Field4">
                <bean:message key="label.author" locale="display"/> <span id="req_111" class="req">*</span>
                </label>
                <div>
                    <html:text styleId="Field4" styleClass="field text medium" property="author"
                                tabindex="3" maxlength="255" />
                </div>
                <logic:messagesPresent property="author">
                    <p class="error"><html:errors property="author" locale="display"/></p>
                </logic:messagesPresent>
            </li>

            <li class="<logic:messagesPresent property='editor'>error</logic:messagesPresent>" id="fo3li0" >
                <label class="desc" id="title5" for="Field5">
                <bean:message key="label.editor" locale="display"/> <span id="req_111" class="req">*</span>
                </label>
                <div>
                    <html:text styleId="Field5" styleClass="field text medium" property="editor"
                                tabindex="4" maxlength="255" />
                </div>
                <logic:messagesPresent property="editor">
                    <p class="error"><html:errors property="editor" locale="display"/></p>
                </logic:messagesPresent>
            </li>

            <li class="<logic:messagesPresent property='price'>error</logic:messagesPresent>" id="fo3li0" >
                <label class="desc" id="title9" for="Field9">
                <bean:message key="label.price" locale="display"/> <span id="req_111" class="req">*</span>
                </label>
                <div>
                    <html:text styleId="Field9" styleClass="field text small" property="price"
                                tabindex="7" maxlength="255" />
                </div>
                <logic:messagesPresent property="price">
                    <p class="error"><html:errors property="price" locale="display"/></p>
                </logic:messagesPresent>
            </li>

            <li class="buttons">
                <html:submit styleId="saveForm" styleClass="btTxt imgStyle" tabindex="8" title="Submit this Details"
                            onclick="actionValue('save')">
                    <bean:message key="button.label.submit" locale="display"/>
                </html:submit>
                <html:submit styleId="saveForm" styleClass="btTxt imgStyle" tabindex="9" title="Submit this Details"
                            onclick="actionValue('cancel')">
                    <bean:message key="button.label.cancel" locale="display"/>
                </html:submit>
            </li>

        </ul>

    </html:form>
</div><!--container-->
</body>
</html>
