<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <bean:message key="label.bookDetails.title" locale="display" />
    </title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <link rel="stylesheet" href="css/theme.css" type="text/css" />
</head>
<body id="public">

<script language="JavaScript" type="text/javascript">
<!--
    function actionValue(value) {
        document.bookForm.action.value = value;
    }
//-->
</script>

<div id="container">

    <%@ include file="/header_inc.jsp" %>

    <html:form action="/book" method="post" styleClass="javachap topLabel" styleId="form3">

        <html:hidden property="bookId" />
        <html:hidden property="action" />
        <div class="info">
            <div class="buttons">
                <html:link tabindex="4" title="Sell a New Book" action="/book" styleClass="positive">
                    <html:img src="images/new_book.png" styleClass="icon" />
                    <bean:message key="link.label.newBook" locale="display" />!
                </html:link>
                <html:link tabindex="5" title="View My Books" action="/bookListing" styleClass="positive">
                    <html:img src="images/new_book.png" styleClass="icon" />
                    <bean:message key="link.label.myBook" locale="display" />!
                </html:link>
            </div>
            <h2>
                <bean:message key="label.bookDetails.heading" locale="display" />
            </h2>
            <p>
                <bean:message key="label.bookDetails.headingCaption" locale="display" />
            </p>
        </div>

        <ul>
            <li class="label">
                <span><bean:message key="label.genre" locale="display"/></span>: <bean:write name="book" property="genre.name" />
            </li>

            <li class="label">
                <span><bean:message key="label.title" locale="display"/></span>: <bean:write name="book" property="title" />
            </li>

            <li class="label">
                <span><bean:message key="label.description" locale="display"/></span>: <bean:write name="book" property="description" />
            </li>

            <li class="label">
                <span><bean:message key="label.author" locale="display"/></span>: <bean:write name="book" property="author" />
            </li>

            <li class="label">
                <span><bean:message key="label.editor" locale="display"/></span>: <bean:write name="book" property="editor" />
            </li>

            <li class="label">
                <span><bean:message key="label.status" locale="display"/></span>: <bean:write name="book" property="status" />
            </li>

            <li class="label">
                <span><bean:message key="label.price" locale="display"/></span>: $
                <logic:lessEqual name="book" property="price" value="10.00">
                    <font color="red">
                        <bean:write name="book" property="price" format="##.##" />
                    </font>
                </logic:lessEqual>
                <logic:greaterThan name="book" property="price" value="10.00">
                    <font color="green">
                        <bean:write name="book" property="price" format="##.##" />
                    </font>
                </logic:greaterThan>
            </li>

        </ul>
        <li class="buttons">
            <html:submit styleId="editForm" styleClass="btTxt imgStyle" tabindex="6"
                        title="Edit Book" onclick="actionValue('edit')">
                <bean:message key="button.label.edit" locale="display" />
            </html:submit>
            <logic:equal name="book" property="status" value="New" >
                <html:submit styleId="publishForm" styleClass="btTxt imgStyle" tabindex="7"
                            title="Publish Book" disabled="false" onclick="actionValue('publish')">
                    <bean:message key="button.label.publish" locale="display" />
                </html:submit>
            </logic:equal>
            <logic:notEqual name="book" property="status" value="New" >
                <html:submit styleId="publishForm" styleClass="btTxt imgStyle" tabindex="7"
                            title="Publish Book" disabled="true">
                    <bean:message key="button.label.publish" locale="display" />
                </html:submit>
            </logic:notEqual>
            <html:submit styleId="deleteForm" styleClass="btTxt imgStyle" tabindex="8"
                        title="Delete Book" onclick="actionValue('delete')">
                <bean:message key="button.label.delete" locale="display" />
            </html:submit>
        </li>

        </html:form>
    </div>

 </body>
 </html>