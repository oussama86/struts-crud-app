/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.domain.impl;

import com.javachap.domain.Genre;

/**
 *
 * @author oussama
 */
public class GenreImpl extends DomainImpl implements Genre {

	private static final long serialVersionUID = -5669471219556818151L;
	
	private String name;

        
        
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("GenreImpl[");
		buffer.append(" name = ").append(name);
		buffer.append("]");
		return buffer.toString();
	}
}

