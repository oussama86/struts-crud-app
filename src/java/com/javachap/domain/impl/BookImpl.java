/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.domain.impl;

import com.javachap.domain.Book;
import com.javachap.domain.Genre;
import com.javachap.domain.User;

/**
 *
 * @author oussama
 */
public class BookImpl extends DomainImpl implements Book {

	private static final long serialVersionUID = 4970184160256555722L;
	
	private String title;
	private String description;
	private String author;
	private String editor;
	private Float price;
	private Genre genre;
	private User owner;
	private String status;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	
        public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
        
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
        

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("BookImpl[");
		buffer.append(" title = ").append(title);
                buffer.append("genre = ").append(genre);
		buffer.append(" description = ").append(description);
		buffer.append(" author = ").append(author);
		buffer.append(" editor = ").append(editor);
		buffer.append(" owner = ").append(owner);
		buffer.append(" price = ").append(price);
		buffer.append(" status = ").append(status);
		buffer.append("]");
		return buffer.toString();
	}
}
