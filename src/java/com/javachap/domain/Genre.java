/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.domain;

/**
 *
 * @author oussama
 */
public interface Genre extends Domain {

	String getName();

	void setName(String name);
        
}
