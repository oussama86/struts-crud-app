/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.domain;

/**
 *
 * @author oussama
 */
public interface Book extends Domain{
	
	enum Status {New, Published}

	String getTitle();

	void setTitle(String title);

	String getDescription();

	void setDescription(String description);

	String getAuthor();

	void setAuthor(String author);

        String getEditor();

	void setEditor(String editor);

        Float getPrice();

	void setPrice(Float price);

	Genre getGenre();

	void setGenre(Genre genre);

	User getOwner();

	void setOwner(User owner);
        
        String getStatus();
	
	void setStatus(String status);	

}
