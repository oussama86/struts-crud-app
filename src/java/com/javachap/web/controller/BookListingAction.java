/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.web.controller;

import com.javachap.domain.Book;
import com.javachap.domain.User;
import com.javachap.service.BookService;
import com.javachap.service.ServiceUtils;
import com.javachap.web.model.BookListingForm;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 *
 * @author oussama
 */
public class BookListingAction extends SecuredAction {

    public ActionForward lmsExecute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response) {

        BookListingForm bookListingForm = (BookListingForm) form;

        if (("delete").equalsIgnoreCase(bookListingForm.getAction())) {

            Long bookId = bookListingForm.getBookId();
            delete(bookId, request);

        }
        else if (("deleteBooks").equalsIgnoreCase(bookListingForm.getAction())) {
            String[] bookIds = bookListingForm.getBookIds();
            for (String bookIdString : bookIds) {

                Long bookId = Long.parseLong(bookIdString);
                delete(bookId, request);
            }
        }

        User user = (User) request.getSession().getAttribute("user");
        List<Book> bookList = ServiceUtils.getBookService().getBooksByUser(user);
        request.setAttribute("bookList", bookList);

        return mapping.getInputForward();
    }

    public void delete(Long bookId, HttpServletRequest request) {
        BookService bookService = ServiceUtils.getBookService();
        boolean deleteSuccessfull = false;
        if(bookId != null && bookId > 0) {
            Book book = bookService.getBook(bookId);
            if (book != null) {
                bookService.delete(book);
                ActionMessages messages = new ActionMessages();
                messages.add(ActionMessages.GLOBAL_MESSAGE,
                                new ActionMessage("message.book.delete"));
                saveMessages(request, messages);
                deleteSuccessfull = true;
            }
        }
        if(!deleteSuccessfull) {
            ActionErrors errors = new ActionErrors();
            errors.add(ActionErrors.GLOBAL_MESSAGE,
                        new ActionMessage("error.book.deleted"));
            saveErrors(request, errors);
        }
    }
}

