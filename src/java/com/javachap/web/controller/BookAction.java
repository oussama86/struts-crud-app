/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.web.controller;

import com.javachap.domain.Genre;
import com.javachap.domain.Book;
import com.javachap.domain.User;
import com.javachap.domain.impl.BookImpl;
import com.javachap.service.BookService;
import com.javachap.service.ServiceUtils;
import com.javachap.web.model.BookForm;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 *
 * @author oussama
 */
public class BookAction extends SecuredAction {

    public ActionForward lmsExecute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response) {
        BookForm bookForm = (BookForm) form;
        ActionForward forward = null;

        List<Genre> genreList = ServiceUtils.getGenreService().getAllGenres();
        request.setAttribute("genreList", genreList);

        System.out.println(bookForm.getAction());
        if (("save").equals(bookForm.getAction())) {
            ActionErrors errors = new ActionErrors();
            if (("Select any Genre").equals(bookForm.getGenre())) {
                errors.add("genre", new ActionMessage("error.label.mandetory"));
            }
            if (bookForm.getTitle().trim().length() < 1 || bookForm.getTitle() == null) {
                errors.add("title", new ActionMessage("error.label.mandetory"));
            }
            if (bookForm.getDescription().trim().length() < 1 || bookForm.getDescription() == null) {
                errors.add("description", new ActionMessage("error.label.mandetory"));
            }
            if (bookForm.getAuthor().trim().length() < 1 || bookForm.getAuthor() == null) {
                errors.add("author", new ActionMessage("error.label.mandetory"));
            }
            if (bookForm.getEditor().trim().length() < 1 || bookForm.getEditor() == null) {
                errors.add("editor", new ActionMessage("error.label.mandetory"));
            }
            if (bookForm.getPrice().trim().length() < 1 || bookForm.getPrice() == null) {
                errors.add("price", new ActionMessage("error.label.mandetory"));
            }
            else {
                try {
                    float priceValue = Float.parseFloat(bookForm.getPrice());
                    if (priceValue < 0) {
                        errors.add("price", new ActionMessage("error.label.greaterThanZero"));
                    }
                }
                catch (NumberFormatException numberFormatException) {
                    errors.add("price", new ActionMessage("error.label.numberOnly"));
                }
            }
            if (!errors.isEmpty()) {
                saveErrors(request, errors);
                forward = mapping.findForward("bookCreateEdit");
            }
            else {
                User user = (User) request.getSession().getAttribute("user");
                Book book = null;
                BookService bookService = ServiceUtils.getBookService();
                Long bookId = bookForm.getBookId();
                if (bookId != null && bookId > 0) {
                    book = bookService.getBook(bookId);
                    ActionMessages messages = new ActionMessages();
                    messages.add(ActionMessages.GLOBAL_MESSAGE,
                                 new ActionMessage("message.book.update"));
                    saveMessages(request, messages);
                }
                else {
                    book = new BookImpl();
                    book.setStatus(Book.Status.New.toString());
                    ActionMessages messages = new ActionMessages();
                    messages.add(ActionMessages.GLOBAL_MESSAGE,
                                 new ActionMessage("message.book.insert"));
                    saveMessages(request, messages);
                }
                Long genreId = Long.parseLong(bookForm.getGenre());
                System.out.println(genreId);
                Genre genre = ServiceUtils.getGenreService().getGenre(genreId);
                System.out.println(genre.getId());
                book.setGenre(genre);
                book.setTitle(bookForm.getTitle());
                book.setDescription(bookForm.getDescription());
                book.setAuthor(bookForm.getAuthor());
                book.setEditor(bookForm.getEditor());
                book.setPrice(Float.parseFloat(bookForm.getPrice()));
                book.setOwner(user);
                book = bookService.save(book);
                forward = mapping.findForward("home");
            }
        }
        else if (("cancel").equalsIgnoreCase(bookForm.getAction())) {
            forward = mapping.findForward("home");
        }
        else if (("edit").equalsIgnoreCase(bookForm.getAction())) {
            Long bookId = bookForm.getBookId();
            Book book = ServiceUtils.getBookService().getBook(bookId);
            String id = String.valueOf(ServiceUtils.getGenreService().getGenre(book.getGenre().getId()).getId());
            bookForm.setGenre(id);
            bookForm.setTitle(book.getTitle());
            bookForm.setDescription(book.getDescription());
            bookForm.setAuthor(book.getAuthor());
            bookForm.setEditor(book.getEditor());
            bookForm.setPrice(String.valueOf(book.getPrice()));
            bookForm.setBookId(bookId);
            forward = mapping.findForward("bookCreateEdit");
        }
        else if (("delete").equalsIgnoreCase(bookForm.getAction())) {
            bookForm.setAction(bookForm.getAction());
            bookForm.setBookId(bookForm.getBookId());
            forward = mapping.findForward("home");
        }
        else if (("publish").equalsIgnoreCase(bookForm.getAction())) {
            BookService bookService = ServiceUtils.getBookService();
            Book book = bookService.getBook( bookForm.getBookId() );
            book.setStatus(Book.Status.Published.toString());
            book = bookService.save(book);
            ActionMessages messages = new ActionMessages();
                    messages.add(ActionMessages.GLOBAL_MESSAGE,
                                 new ActionMessage("message.book.publish"));
                    saveMessages(request, messages);
            forward = mapping.findForward("home");
        }
        else if (bookForm.getBookId() != null) {
            Book book = ServiceUtils.getBookService().getBook(bookForm.getBookId());
            request.setAttribute("book", book);
            forward = mapping.getInputForward();
        }
        else {
            forward = mapping.findForward("bookCreateEdit");
        }
        return forward;
    }
}

