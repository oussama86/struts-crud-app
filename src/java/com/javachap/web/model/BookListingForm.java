/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.web.model;

/**
 *
 * @author oussama
 */
public class BookListingForm extends BaseForm {

	private static final long serialVersionUID = -6519972467246625077L;

	/**
     * BookId
     */
    protected Long bookId = null;

    public void setBookId(Long bookId){
        this.bookId = bookId;
    }

    public Long getBookId(){
       return (this.bookId);
    }


    /**
     * LeadIds
     */
    protected String[] bookIds = null;

    public void setBookIds(String[] bookIds){
        this.bookIds = bookIds;
    }

    public String[] getBookIds(){
       return (this.bookIds);
    }

}