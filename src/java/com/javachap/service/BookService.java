/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.service;

import com.javachap.domain.Book;
import com.javachap.domain.User;
import java.util.List;

/**
 *
 * @author oussama
 */
public interface BookService extends Service {

	Book getBook(Long id);
	
	Book save(Book book);
	
	void delete(Book book);
	
	List<Book> getBooksByUser(User user);	
}