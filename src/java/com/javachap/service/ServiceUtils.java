package com.javachap.service;

import com.javachap.service.impl.BookServiceImpl;
import com.javachap.service.impl.GenreServiceImpl;
import com.javachap.service.impl.UserServiceImpl;

public class ServiceUtils {

	/**
	 * Gets Instance of UserService
	 * @return UserService Instance
	 */	
	public static UserService getUserService(){
		return UserServiceImpl.getInstance();
	}	
	
	/**
	 * Gets Instance of BookService
	 * @return BookService Instance
	 */
	public static BookService getBookService(){
            return BookServiceImpl.getInstance();
                
        }
	
	/**
	 * Gets Instance of CategoryService
	 * @return CategoryService Instance
	 */	
	public static GenreService getGenreService(){
		return GenreServiceImpl.getInstance();
	}
	
}
