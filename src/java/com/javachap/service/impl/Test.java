package com.javachap.service.impl;

import java.util.List;

import com.javachap.domain.Genre;
import com.javachap.domain.Book;
import com.javachap.domain.User;
import com.javachap.domain.impl.GenreImpl;
import com.javachap.domain.impl.BookImpl;
import com.javachap.domain.impl.UserImpl;
import com.javachap.service.GenreService;
import com.javachap.service.BookService;
import com.javachap.service.ServiceUtils;
import com.javachap.service.UserService;

public class Test {
	
	public static void main(String args[]) {
		
		UserService userService = ServiceUtils.getUserService();
		// User Authenticate
		User user =  userService.authenticate("user@javachap.com", "javachap");
		System.out.println("User:" + user);
		
		// User Save
		user = new UserImpl();
		user.setFirstName("java");
		user.setLastName("chap");
		user.setEmail("email"+System.currentTimeMillis());
		user.setPassword("test");
		userService.save(user);
		
		// User Get
		user = userService.getUser(user.getId());
		System.out.println("User:" + user);

		
		GenreService genreService = ServiceUtils.getGenreService();
		
		// Genre Save
		Genre genre = new GenreImpl();
		genre.setName("Test Name_" + System.currentTimeMillis());
		genreService.save(genre);
		
		genre = genreService.getGenre("Drama");
		System.out.println(genre);
		
		List<Genre> genres = genreService.getAllGenres();
		System.out.println(genres);
		
		System.out.println(genre.getId());
                genre = genreService.getGenre(genre.getId());
		System.out.println(genre);
		
		BookService bookService = ServiceUtils.getBookService();
		Book book = new BookImpl();
		book.setTitle("Test Title");
		book.setDescription("Test Description");
		book.setAuthor("Test");
		book.setEditor("test");
		book.setPrice(10.2f);
		book.setStatus(Book.Status.Published.toString());
		book.setGenre(genre);
		book.setOwner(user);
		bookService.save(book);

                book = bookService.getBook(book.getId());
		System.out.println(book);		
	}
}
