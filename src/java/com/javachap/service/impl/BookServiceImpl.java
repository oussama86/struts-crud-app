/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.service.impl;

import com.javachap.domain.Book;
import com.javachap.domain.User;
import com.javachap.service.BookService;
import com.javachap.utils.HibernateUtils;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author oussama
 */
public class BookServiceImpl extends ServiceImpl implements BookService {

	private static final long serialVersionUID = 872905902784301463L;

	private static final String BOOKS_BY_USER_QUERY = 
			"from Book book where book.owner.id = :UserId";
	
	/**
	 * Singleton Instance of BookServiceImpl
	 */
	private static BookServiceImpl bookServiceImpl = new BookServiceImpl();
	
	/**
	 * Creates Instance of {@link BookServiceImpl}
	 */
	private BookServiceImpl(){		
	}
	
	/***
	 * Gets Instance of BookService
	 * @return BookService Implementation
	 */
	public static BookServiceImpl getInstance(){	
		return bookServiceImpl;
	}
	
	/* (non-Javadoc)
	 * @see com.javachap.service.BookService#delete(com.javachap.domain.Book)
	 */
	public void delete(Book book) {
		Session session = HibernateUtils.currentSession();
		Transaction tx = null;
		boolean rollback = true;
		try {
		     tx = session.beginTransaction();
		     session.delete(book);
		     tx.commit();
		     rollback = false;
		 }
		 catch (Exception e) {
		     throw new ServiceException(e);
		 }
		 finally {
			 if( rollback && tx != null){ 
		    	 tx.rollback();
			 }			 
			 HibernateUtils.closeSession();
		 }
	}

	/* (non-Javadoc)
	 * @see com.javachap.service.BookService#getBook(java.lang.Long)
	 */
	public Book getBook(Long id) {
		Book book = null;
		try {
			Session session = HibernateUtils.currentSession();
			book = (Book)session.get(Book.class, id);
			HibernateUtils.closeSession();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		return book;	
	}

	/* (non-Javadoc)
	 * @see com.javachap.service.BookService#save(com.javachap.domain.Book)
	 */
	public Book save(Book book) {
		Session session = HibernateUtils.currentSession();
		Transaction tx = null;
		boolean rollback = true;
		try {
		     tx = session.beginTransaction();
		     session.saveOrUpdate(book);
		     tx.commit();
		     rollback = false;
		 }
		 catch (Exception e) {
		     throw new ServiceException(e);
		 }
		 finally {
			 if( rollback && tx != null){ 
		    	 tx.rollback();
			 }			 
			 HibernateUtils.closeSession();
		 }
		return book;
	}
	
	/* (non-Javadoc)
	 * @see com.javachap.service.BookService#getBooksByUser(com.javachap.domain.User)
	 */
	@SuppressWarnings("unchecked")
	public List<Book> getBooksByUser(User user) {
		List<Book> books = null; 
		try {
			Session session = HibernateUtils.currentSession();
			Query query = session.createQuery(BOOKS_BY_USER_QUERY);
			query.setParameter("UserId", user.getId());
			books = (List<Book>) query.list();
			HibernateUtils.closeSession();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		return books;	
        }	
}

