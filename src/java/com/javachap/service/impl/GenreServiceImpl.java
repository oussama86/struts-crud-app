/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.service.impl;

import com.javachap.domain.Genre;
import com.javachap.service.GenreService;
import com.javachap.utils.HibernateUtils;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author oussama
 */
public class GenreServiceImpl extends ServiceImpl implements GenreService {

	private static final long serialVersionUID = 380026904541710181L;
	
	private String ALL_GENRES_QUERY = "from Genre genre";
	
	private String GENRE_BY_NAME_QUERY = 
		"from Genre genre where genre.name = :GenreName";
	
	/**
	 * Singleton Instance of GenreServiceImpl
	 */
	private static GenreServiceImpl genreServiceImpl = new GenreServiceImpl();
	
	/**
	 * Creates Instance of {@link GenreServiceImpl}
	 */
	private GenreServiceImpl(){		
	}
	
	/***
	 * Gets Instance of GenreService
	 * @return GenreService Implementation
	 */
	public static GenreService getInstance(){	
		return genreServiceImpl;
	}	

	/* (non-Javadoc)
	 * @see com.javachap.service.CategoryService#getAllCategories()
	 */
	@SuppressWarnings("unchecked")
	public List<Genre> getAllGenres() {
		List<Genre> genres = null; 
		try {
			Session session = HibernateUtils.currentSession();
			Query query = session.createQuery(ALL_GENRES_QUERY);
			genres = (List<Genre>) query.list();
			HibernateUtils.closeSession();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		return genres;	
	}

	/* (non-Javadoc)
	 * @see com.javachap.service.GenreService#getCategory(java.lang.Long)
	 */
	public Genre getGenre(Long genreId) {
		Genre genre = null;
		try {
			Session session = HibernateUtils.currentSession();
			genre = (Genre)session.get(Genre.class, genreId);
			HibernateUtils.closeSession();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	
                return genre;
	}

	/* (non-Javadoc)
	 * @see com.javachap.service.GenreService#getCategory(java.lang.String)
	 */
	public Genre getGenre(String genreName) {
		Genre genre = null;
		try {
			Session session = HibernateUtils.currentSession();
			Query query = session.createQuery(GENRE_BY_NAME_QUERY);
			query.setString("GenreName", genreName);
			@SuppressWarnings("unchecked")
			List<Genre> list = (List<Genre>) query.list();
			if (list.size() == 1) {
				genre = (Genre) list.get(0);
			}
			HibernateUtils.closeSession();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		return genre;		
	}

	/* (non-Javadoc)
	 * @see com.javachap.service.GenreService#save(com.javachap.domain.Genre)
	 */
	public Genre save(Genre genre) {
		Session session = HibernateUtils.currentSession();
		Transaction tx = null;
		boolean rollback = true;
		try {
		     tx = session.beginTransaction();
		     session.save(genre);
		     tx.commit();
		     rollback = false;
		 }
		 catch (Exception e) {
		     throw new ServiceException(e);
		 }
		 finally {
			 if( rollback && tx != null){ 
		    	 tx.rollback();
			 }			 
			 HibernateUtils.closeSession();
		 }
		return genre;		
	}
}

