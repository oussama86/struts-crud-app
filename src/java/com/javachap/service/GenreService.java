/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.javachap.service;

import com.javachap.domain.Genre;
import java.util.List;

/**
 *
 * @author oussama
 */
public interface GenreService extends Service {

	Genre getGenre(Long genreId);
	
	Genre getGenre(String genreName);
	
	Genre save(Genre genre);
	
	List<Genre> getAllGenres();
}