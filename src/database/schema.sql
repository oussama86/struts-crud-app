DROP DATABASE IF EXISTS books;

CREATE DATABASE books;

GRANT ALL PRIVILEGES ON books.* TO 'user'@'localhost' IDENTIFIED BY 'user' WITH GRANT OPTION;

USE books;

# -----------------------------------------------------------------------
# BOOKUSER
# -----------------------------------------------------------------------
CREATE TABLE USER
(
   USR_ID BIGINT NOT NULL AUTO_INCREMENT,
   USR_FIRST_NAME VARCHAR(255)  NOT NULL,
   USR_LAST_NAME VARCHAR(255) NOT NULL,
   USR_EMAIL VARCHAR(255) NOT NULL,
   USR_PASSWORD VARCHAR(255) NOT NULL,
   USR_CREATED_DATE DATETIME,
   USR_MODIFIED_DATE DATETIME,
      PRIMARY KEY(USR_ID),
      UNIQUE(USR_EMAIL)
   ) ENGINE = InnoDB;


# -----------------------------------------------------------------------
# GENRE
# -----------------------------------------------------------------------
CREATE TABLE GENRE
(
   GE_ID BIGINT NOT NULL AUTO_INCREMENT,
   GE_NAME VARCHAR(255) NOT NULL,
   GE_CREATED_DATE DATETIME,
   GE_MODIFIED_DATE DATETIME,
      PRIMARY KEY(GE_ID) ,
      UNIQUE(GE_NAME)
   ) engine=InnoDB;

# -----------------------------------------------------------------------
# BOOK
# -----------------------------------------------------------------------
CREATE TABLE BOOK
(
   BO_ID BIGINT NOT NULL AUTO_INCREMENT,
   BO_TITLE VARCHAR(255) NOT NULL,
   BO_DESCRIPTION VARCHAR(400) NOT NULL,
   BO_AUTHOR VARCHAR(255) NOT NULL,
   BO_EDITOR VARCHAR(255) NOT NULL,
   BO_PRICE DECIMAL(19,2),
   BO_STATUS VARCHAR(100) NOT NULL,
   BO_OWNER_ID BIGINT NOT NULL,
   BO_GENRE_ID BIGINT NOT NULL,
   BO_CREATED_DATE DATETIME,
   BO_MODIFIED_DATE DATETIME,
      PRIMARY KEY(BO_ID),
      FOREIGN KEY (BO_OWNER_ID) REFERENCES USER(USR_ID),
      FOREIGN KEY (BO_GENRE_ID) REFERENCES GENRE(GE_ID)
   )engine=InnoDB;



INSERT INTO USER (USR_FIRST_NAME, USR_LAST_NAME, USR_EMAIL, USR_PASSWORD, USR_CREATED_DATE, USR_MODIFIED_DATE)
VALUES('Oussama', 'Smiai', 'user@struts.com', 'struts', now(), now());


INSERT INTO GENRE (GE_NAME) VALUES('Romance');
INSERT INTO GENRE (GE_NAME) VALUES('Drama');
INSERT INTO GENRE (GE_NAME) VALUES('Historical');
INSERT INTO GENRE (GE_NAME) VALUES('Myth');
